import React, { FC } from 'react';
import { Styled } from 'direflow-component';
import styles from './App.css';

interface IProps {
  imagen: string;
  fecha_de_construccion: string;
  dimensiones: number;
  numero_de_habitaciones: number;
  observaciones: string;
  disponible: boolean;
}

const App: FC<IProps> = (props) => {
  
  return (
    <Styled styles={styles}>
      <div className='app'>
        <div className="imageContainer">
          <img src={props.imagen} alt="imagen" />
        </div>
        <div className="propertyContent">
          <div className="propertyInfo">

            <p> { props.fecha_de_construccion } </p>
            <p> { props.dimensiones } m2 </p>
            <p> { props.numero_de_habitaciones } habitación/es </p>
            <p> { props.disponible ? 'Disponible' : 'No disponible' } </p>

          </div>

          <div className="propertyDescription">
            <p> {props.observaciones} </p>
          </div>
        </div>
      </div>
    </Styled>
  );
};

App.defaultProps = {
  imagen: 'https://cdn.pixabay.com/photo/2016/06/24/10/47/house-1477041_960_720.jpg',
  fecha_de_construccion: '06/06/2021',
  dimensiones: 450,
  numero_de_habitaciones: 7,
  observaciones: 'Muy buen edificio y lujoso, con amplias vistas de toda la ciudad',
  disponible: true
}

export default App;
